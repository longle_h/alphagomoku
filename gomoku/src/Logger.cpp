#include "Logger.h"


Logger::Logger()
{
}


Logger::~Logger()
{
}

std::fstream Logger::open_file()
{
	std::fstream fs(FILE_LOG, std::fstream::app | std::fstream::out);	
	return fs;
}

void Logger::message_received(const std::string& mess)
{
	std::fstream fs = Logger::open_file();

	fs << "Message reveived : [" << mess << "]" << std::endl;
	fs.close();
}

void Logger::message_send(const std::string& mess)
{
	std::fstream fs = Logger::open_file();

	fs << "Message send : [" << mess << "]" << std::endl;
	fs.close();
}

void Logger::other_log(const std::string& mess)
{
	std::fstream fs = Logger::open_file();

	fs << "Other : [" << mess << "]" << std::endl;
	fs.close();
}
