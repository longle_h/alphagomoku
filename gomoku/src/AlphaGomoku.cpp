#include "AlphaGomoku.h"
#include "Helper.h"
#include <sstream>
#include <cstdlib>
#include <sstream>
#include <Logger.h>


AlphaGomoku::AlphaGomoku() : _board(BoardGomoku())
{
	this->map_size = 0;
	call_map["START"] = &AlphaGomoku::brain_start;
	call_map["INFO"] = &AlphaGomoku::brain_info;
	call_map["TURN"] = &AlphaGomoku::brain_turn;
	call_map["BEGIN"] = &AlphaGomoku::brain_begin;
	call_map["END"] = &AlphaGomoku::brain_end;
	call_map["RESTART"] = &AlphaGomoku::brain_restart;
	call_map["RECSTART"] = &AlphaGomoku::brain_recstart;
	call_map["TAKEBACK"] = &AlphaGomoku::brain_takeback;
	call_map["PLAY"] = &AlphaGomoku::brain_play;
	call_map["ABOUT"] = &AlphaGomoku::brain_about;
	//On ne re�oit pas la commande board
	this->board_state = false;
	//Pour notre algorithme, on ne prend pas en compte notre tour de jeu
	//On a seulement besoin de savoir si cette case nous appartient ou non
	this->my_char = '1';
	this->other_char = '2';

}


AlphaGomoku::~AlphaGomoku()
{
}

std::unordered_map <std::string, AlphaGomoku::func> AlphaGomoku::get_call_map() const
{
	return this->call_map;
}



std::string AlphaGomoku::brain_start(std::vector<std::string> argv)
{
	if (argv.size() == 2)
	{
		this->map_size = std::atoi(argv[1].c_str());
		_board.fillMap(this->map_size);
		return "OK";	
	}
	return "ERROR - No map size given";
}

std::string AlphaGomoku::go_map(std::vector<std::string> argv)
{
	if (argv.size() > 0)
	{
		if (argv[0].compare("BOARD") == 0)
			this->board_state = true;
		else if (argv[0].compare("DONE") == 0 && this->board_state)
		{
			std::string ret = this->brain_turn(argv);
			this->board_state = false;
			return ret;
		}
		else if (this->board_state)
			this->brain_board(argv);
		else if (!this->board_state)
		{
			auto iter = this->call_map.find(argv[0]);
			if (iter != this->call_map.end())
			{
				//AAARGH cette ligne u_u
				std::string ret = (this->*call_map[argv[0]])(argv);
				/*if (!ret.empty())
					Logger::message_send(ret);*/
				return ret;
			}
			
		}
		else
		{
			//Logger::other_log("Error cannot find the callback for : " + argv[0]);
			return "ERROR unknow command";
		}
		return "";
	}
	return "";
}

std::string AlphaGomoku::brain_info(std::vector<std::string> argv)
{
	return "";
}

std::string AlphaGomoku::brain_turn(std::vector<std::string> argv)
{
	if (argv.size() != 2 && !this->board_state)
		return "ERROR - Not enought arguments";
	unsigned int pos_x = 0;
	unsigned int pos_y = 0;
	if (!this->board_state)
	{
		std::vector<std::string> pos = Helper::split(argv[1], ',');
		if (pos.size() != 2)
			return "ERROR - Format is not x,y";
		pos_x = std::atoi(pos[0].c_str());
		pos_y = std::atoi(pos[1].c_str());
		try {
			this->_board.put_on_coord(pos_x, pos_y, this->other_char);
		}
		catch (std::logic_error e) {
			return std::string("ERROR - out of range");
		}
	}
	Node _baseNode = Node(0, 0);

    this->_board.get_possible_turn(_baseNode, true);
    BoardGomoku tmp;
    Node adversaireNode = Node(0,0);
    tmp < this->_board;
    tmp.get_possible_turn(adversaireNode, true);

    int i_a = get_index_to_play(adversaireNode);
    int i_b = get_index_to_play(_baseNode);
    if (_baseNode.children.at(i_b).value < adversaireNode.children.at(i_a).value)
    {
        pos_x = adversaireNode.children.at(i_a).x;
        pos_y = adversaireNode.children.at(i_a).y;
    }
    else
    {
        pos_x = _baseNode.children.at(i_b).x;
        pos_y = _baseNode.children.at(i_b).y;
    }


	/*this->manage_minmax(_baseNode);
	int w = this->get_index_to_play(_baseNode);
	pos_x = _baseNode.children.at(w).x;
	pos_y = _baseNode.children.at(w).y;
    std::stringstream ss;
    ss << "Value max choisie : " << _baseNode.children.at(w).value;
    Logger::other_log(ss.str());*/
	try {
		this->_board.put_on_coord(pos_x, pos_y, this->my_char);
	}
	catch (std::logic_error e) {
		return std::string("ERROR - out of range");
	}
	return this->pos_to_string(pos_x, pos_y);
}

std::string AlphaGomoku::brain_begin(std::vector<std::string> argv)
{
	unsigned int x;
	unsigned int y;

	if (this->map_size < 5)
		return "ERROR - map_size if too small";
	x = this->map_size / 2;
	y = this->map_size / 2;
	try {
		this->_board.put_on_coord(x, y, this->my_char);
	}
	catch (std::logic_error e) {
		return "ERROR - out of range";
	}
	return this->pos_to_string(x, y);
}

std::string AlphaGomoku::brain_end(std::vector<std::string> argv)
{
	return "";
}

std::string AlphaGomoku::brain_restart(std::vector<std::string> argv)
{
	this->_board.fillMap(this->map_size);
	return "OK";
}

std::string AlphaGomoku::brain_recstart(std::vector<std::string> argv)
{
	return "ERROR - rectangular board is not supported";
}

std::string AlphaGomoku::brain_takeback(std::vector<std::string> argv)
{
	if (argv.size() != 2)
		return "ERROR - Not enought arguments";
	else
	{
		std::vector<std::string> pos = Helper::split(argv[1], ',');
		if (pos.size() != 2)
			return "ERROR - Format is not x,y";
		unsigned int pos_x = std::atoi(pos[0].c_str());
		unsigned int pos_y = std::atoi(pos[1].c_str());
		try {
			this->_board.put_on_coord(pos_x, pos_y, '0');
		}
		catch (std::logic_error e) {
			return std::string("ERROR - out of range");
		}
	}
	return "OK";
}

std::string AlphaGomoku::brain_play(std::vector<std::string> argv)
{
	if (argv.size() != 2)
		return "ERROR - Not enought arguments";
	else
	{
		std::vector<std::string> pos = Helper::split(argv[1], ',');
		if (pos.size() != 2)
			return "ERROR - Format is not x,y";
		unsigned int pos_x = std::atoi(pos[0].c_str());
		unsigned int pos_y = std::atoi(pos[1].c_str());
		try {
			this->_board.put_on_coord(pos_x, pos_y, '0');
		}
		catch (std::logic_error e) {
			return std::string("ERROR - out of range");
		}
		return this->pos_to_string(pos_x, pos_y);
	}
}

std::string AlphaGomoku::brain_about(std::vector<std::string> argv)
{
	return "name=\"AlphaGomoku\", version=\"0.1\", author=\"Henri Longle, Sebastien Bruere\", country=\"France\"";
}

std::string AlphaGomoku::brain_board(std::vector<std::string> argv)
{
	if (argv.size() != 1)
		return "ERROR - Not enought arguments";
	else
	{
		std::vector<std::string> pos = Helper::split(argv[0], ',');
		if (pos.size() != 3)
			return "ERROR - Format is not x,y,char";
		unsigned int pos_x = std::atoi(pos[0].c_str());
		unsigned int pos_y = std::atoi(pos[1].c_str());
		char c = (pos[2].compare("1") == 0) ? this->my_char : this->other_char;
		try {
			this->_board.put_on_coord(pos_x, pos_y, c);
		}
		catch (std::logic_error e) {
			return std::string("ERROR - out of range");
		}
	}
	return "";
}

std::string AlphaGomoku::pos_to_string(unsigned int x, unsigned int y)
{
	std::stringstream sstring;
	sstring << x << "," << y;
	return sstring.str();
}


void AlphaGomoku::manage_minmax(Node & _baseNode)
{
    this->_board.get_possible_turn(_baseNode, false);

    //Condition de victoire
    /*for (auto it = _baseNode.children.begin(); it != _baseNode.children.end(); it++) {
        if (_board.evaluate((*it).x,(*it).y, true) > 1000) {
            Logger::other_log("IN VICTORY CONDITION@@@@@@@@@@@@");
            (*it).value = 1000;
        }
    }*/


    //Inversion de la map
        for (auto it = _baseNode.children.begin(); it != _baseNode.children.end(); it++)
	{
        BoardGomoku tmp;
        tmp < this->_board;
        tmp.put_on_coord((*it).x, (*it).y, this->my_char);
        tmp.get_possible_turn((*it), false);
 		for (auto it2 = (*it).children.begin(); it2 != (*it).children.end(); it2++)
        {
            BoardGomoku tmp2;
            tmp2 < tmp;
            tmp2.put_on_coord((*it2).x, ((*it2).y), this->other_char);
            tmp2.get_possible_turn((*it2), true);
        }
	}

    for (auto it = _baseNode.children.begin(); it != _baseNode.children.end(); it++)
    {
        for (auto it2 = (*it).children.begin(); it2 != (*it).children.end(); it2++)
        {
            (*it2).value = (*it2).children.at(this->get_index_to_play((*it2))).value;
        }
        (*it).value = (*it).children.at(this->get_min((*it))).value;
    }


}

int AlphaGomoku::get_index_to_play(Node _baseNode)
{
	std::vector<unsigned int> _v;
	int max = -1;
	auto it = _baseNode.children.begin();
 	unsigned int i = 0;
	for (; it != _baseNode.children.end(); it++)
	{
		if ((*it).value > max)
		{
			max = (*it).value;
			_v.clear();
			_v.push_back(i);
		}
		else if ((*it).value == max)
			_v.push_back(i);
		i++;
	};

	if (_v.size() == 1)
		return _v.at(0);
	else
		return _v.at(std::rand() % (_v.size() - 1));
}

int AlphaGomoku::get_min(Node _baseNode)
{
	std::vector<unsigned int> _v;
	int min = 1000000;
	auto it = _baseNode.children.begin();
	unsigned int i = 0;
	for (; it != _baseNode.children.end(); it++)
	{
		if ((*it).value < min)
		{
			min = (*it).value;
			_v.clear();
			_v.push_back(i);
		}
		else if ((*it).value == min)
			_v.push_back(i);
		i++;
	};

	if (_v.size() == 1)
		return _v.at(0);
	else
		return _v.at(std::rand() % (_v.size() - 1));
}