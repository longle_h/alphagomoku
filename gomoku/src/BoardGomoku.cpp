#include "BoardGomoku.h"
#include <sstream>
#include <Logger.h>


BoardGomoku::BoardGomoku()
{
}

BoardGomoku::~BoardGomoku()
{
}

void BoardGomoku::fillMap(const int & map_size)
{
	this->map.clear();
	this->size = map_size;
    int x = 0;
    int y = 0;

	for (; x < map_size; x++)
	{
		this->map.emplace_back();
		y = 0;
		for (; y < map_size; y++)
		{
			this->map[x].push_back('0');
		}
	}
}

void BoardGomoku::put_on_coord(const int & x, const int & y, const char & c) throw (std::logic_error)
{
	if (x >= 0 && y >= 0)
	{
		if (x < this->size && y < this->size)
		{
			this->map.at(x).at(y) = c;
			return;
		}
	}
	throw std::logic_error("x and y are out of range");
}

void BoardGomoku::dumpMap()
{
	for (auto it = map.begin(); it != map.end() ; it++)
	{
		for (auto it2 = (*it).begin(); it2 != (*it).end(); it2++)
		{
			std::cout << (*it2);
		}
		std::cout << std::endl;
	}
}

bool BoardGomoku::is_free(const int & x, const int & y)
{
	if (x >= 0 && y >= 0)
	{
		if (x < this->size && y < this->size)
			return (this->map.at(x).at(y) == '0');
	}
	return (false);
}

void BoardGomoku::get_possible_turn(Node & base_node, bool evaluate)
{
    int x = 0;
    int y = 0;
	for (; x < this->size; x++)
	{
		y = 0;
		for (; y < this->size; y++)
		{
			if (!is_free(x, y))
				this->add_adjacent_node(x, y, base_node, evaluate);
		}
	}
}

int BoardGomoku::getSize() const
{
	return this->size;
}

std::vector<std::vector<char>> BoardGomoku::getMap() const
{
	return this->map;
}

void BoardGomoku::setMap(std::vector<std::vector<char>> _v)
{
	this->map = _v;
}

void BoardGomoku::setSize(int size)
{
	this->size = size;
}

void BoardGomoku::add_adjacent_node(int x, int y, Node & base_node, bool evaluate)
{
	/* x - 1/ y - 1 | x - 1/  y | x - 1 | y + 1 
	   x / y - 1 | x /  y | x  | y + 1 
	   x + 1/ y - 1 | x + 1/  y | x + 1 | y + 1
	   */
	if (x > 0)
	{
		if (y > 0 && is_free(x - 1, y - 1) && non_existing_node(x - 1, y - 1, base_node))
		{
			base_node.children.emplace_back(x - 1, y - 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
		if ((y + 1) < size && is_free(x - 1, y + 1) && non_existing_node(x - 1, y + 1, base_node))
		{
			base_node.children.emplace_back(x - 1, y + 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
		if (is_free(x - 1, y) && non_existing_node(x - 1, y, base_node))
		{
			base_node.children.emplace_back(x - 1, y);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
	}
	if ((x  + 1) < this->size)
	{
		if (y > 0 && is_free(x + 1, y - 1) && non_existing_node(x + 1, y - 1, base_node))
		{
			base_node.children.emplace_back(x + 1, y - 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
		if ((y + 1) < size && is_free(x + 1, y + 1) && non_existing_node(x + 1, y + 1, base_node))
		{
			base_node.children.emplace_back(x + 1, y + 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
		if (is_free(x + 1, y) && non_existing_node(x + 1, y, base_node))
		{
			base_node.children.emplace_back(x + 1, y);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
	}
	if (y > 0)
	{
		if (is_free(x, y - 1) && non_existing_node(x, y - 1, base_node))
		{
			base_node.children.emplace_back(x, y - 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
	}
	if ((y + 1) < this->size)
	{
		if (is_free(x, y + 1) && non_existing_node(x, y + 1, base_node))
		{
			base_node.children.emplace_back(x, y + 1);
			base_node.children.back().value = this->evaluate(base_node.children.back().x, base_node.children.back().y, evaluate);
		}
	}
}

bool BoardGomoku::non_existing_node(int x, int y, Node & base_node)
{
	for (auto it = base_node.children.begin(); it != base_node.children.end(); it++)
	{
		if ((*it).x == x && (*it).y == y)
			return false;
	}
	return true;
}

int BoardGomoku::evaluate(int x, int y, bool evaluate)
{
    int total = 0;
    if (!evaluate)
        return -1;
    if (this->check_number_vertical(x, y, '1') >= 4)
        total += 2000;
    else if (this->check_number_horizontal(x, y, '1') >= 4)
        total += 2000;
    else if (this->check_number_diagonal_desc(x, y, '1') >= 4)
        total += 2000;
    else if (this->check_number_diagonal_asc(x, y, '1') >= 4)
        total += 2000;

    if (this->check_number_vertical(x, y, '1') == 3 && this->check_vertical_surround(x, y, '1', '2') == 0)
        total += 1000;
    if (this->check_number_diagonal_asc(x, y, '1') == 3 && this->check_diag_asc_surround(x,y,'1','2') == 0)
        total += 1000;
    if (this->check_number_diagonal_desc(x, y, '1') == 3 && this->check_diag_desc_surrond(x,y,'1','2') == 0)
        total += 1000;
    if (this->check_number_horizontal(x, y, '1') == 3 && this->check_horizontal_surround(x,y,'1','2') == 0)
        total += 1000;

    if (this->check_number_vertical(x, y, '1') == 3)
        total += 90;
    else if (this->check_number_vertical(x, y, '1') == 2)
        total += 50;
    else if (this->check_number_vertical(x, y, '1') == 1)
        total += 10;

    if (this->check_number_diagonal_asc(x, y, '1') == 3)
        total += 90;
    else if (this->check_number_diagonal_asc(x, y, '1') == 2)
        total += 50;
    else if (this->check_number_diagonal_asc(x, y, '1') == 1)
        total += 10;

    if (this->check_number_diagonal_desc(x, y, '1') == 3)
        total += 90;
    else if (this->check_number_diagonal_desc(x, y, '1') == 2)
        total += 50;
    else if (this->check_number_diagonal_desc(x, y, '1') == 1)
        total += 10;

    if (this->check_number_horizontal(x, y, '1') == 3)
        total += 90;
    else if (this->check_number_horizontal(x, y, '1') == 2)
        total += 50;
    else if (this->check_number_horizontal(x, y, '1') == 1)
        total += 10;
    return total;
}

bool BoardGomoku::is_win(int x, int y)
{
	return false;
}

int BoardGomoku::check_number_diagonal_desc(int x, int y, char c) {
    int sy = y;
    int sx = x;
    int ret = 0;

    if (x > 0)
        x -= 1;
    if (y > 0)
        y -= 1;
	while ((x >= 0 && y >= 0) && this->map.at(x).at(y) == c) {
		ret++;
		y--;
		x--;
	}
	x = sx + 1;
	y = sy + 1;
	while ((x < this->size && y < this->size) && this->map.at(x).at(y) == c) {
		ret++;
		y++;
		x++;
	}
	return ret;
}

int BoardGomoku::check_number_diagonal_asc(int x, int y, char c) {
    int sy = y;
    int sx = x;
    int ret = 0;

    if (sx > 0)
        x -= 1;
    y += 1;
	while ((x >= 0 && y < this->size) && this->map.at(x).at(y) == c) {
		ret++;
		y++;
		x--;
	}
	x = sx + 1;
    if (sy > 0)
	    y = sy - 1;
	while ((x < this->size && y >= 0) && this->map.at(x).at(y) == c) {
		ret++;
		y--;
		x++;
	}
	return ret;
}

int BoardGomoku::check_number_vertical(int x, int y, char c)
{
    int sy = y;
    int ret = 0;
    if (sy > 0)
	    sy--;
	while (sy >= 0 && this->map.at(x).at(sy) == c)
	{
		sy--;
		ret++;
		if (sy == 0)
			break;
	}
	sy = y + 1;
	while (sy < this->size && this->map.at(x).at(sy) == c)
	{
		sy++;
		ret++;
	}
	return ret;
}

int BoardGomoku::check_number_horizontal(int x, int y, char c)
{
    int sx = x;
    int ret = 0;
	if (sx > 0)
		sx--;
	while (sx >= 0 && this->map.at(sx).at(y) == c)
	{
		sx--;
		ret++;
		if (sx == 0)
			break;
	}
	sx = x  + 1;
	while (sx < this->size && this->map.at(sx).at(y) == c )
	{
		sx++;
		ret++;
	}
	return ret;
}

int BoardGomoku::check_vertical_surround(int x, int y, char c, char i1) {
    int sy = y;
    int ret = 0;
    if (sy > 0)
        sy--;
    while (sy >= 0 && this->map.at(x).at(sy) == c)
        sy--;
    if (sy >= 0 && this->map.at(x).at(sy) == i1)
        ret++;
    sy = y + 1;
    while (sy < this->size && this->map.at(x).at(sy) == c)
        sy++;
    if (sy < this->size && this->map.at(x).at(sy) == i1)
        ret++;
    return ret;
}

int BoardGomoku::check_diag_asc_surround(int x, int y, char c, char c2) {
    int sy = y;
    int sx = x;
    int ret = 0;

    if (x > 0)
        x -= 1;
    y += 1;
    while ((x >= 0 && y < this->size) && this->map.at(x).at(y) == c) {
        y++;
        x--;
    }
    if (x >= 0 && y < this->size && this->map.at(x).at(y) == c2)
        ret++;
    x = sx + 1;
    if (y > 0)
        y = sy - 1;
    while ((x < this->size && y >= 0) && this->map.at(x).at(y) == c) {
        y--;
        x++;
    }
    if (x < this->size && y >= 0 && this->map.at(x).at(y) == c2)
        ret++;
    return ret;
}

int BoardGomoku::check_diag_desc_surrond(int x, int y, char c, char c2) {
    int sy = y;
    int sx = x;
    int ret = 0;

    if (x > 0)
        x -= 1;
    if (y > 0)
        y -= 1;
    while ((x >= 0 && y >= 0) && this->map.at(x).at(y) == c) {
        y--;
        x--;
    }
    if (x >= 0 && y >= 0 && this->map.at(x).at(y) == c2)
        ret++;
    x = sx + 1;
    y = sy + 1;
    while ((x < this->size && y < this->size) && this->map.at(x).at(y) == c) {
        y++;
        x++;
    }
    if (x < this->size && y < this->size && this->map.at(x).at(y) == c2)
        ret++;
    return ret;
}

int BoardGomoku::check_horizontal_surround(int x, int y, char c, char c2) {
    int sx = x;
    int ret = 0;
    if (sx != 0)
        sx--;
    while (sx >= 0 && this->map.at(sx).at(y) == c)
        sx--;
    if (sx >= 0 && this->map.at(sx).at(y) == c2)
        ret++;
    sx = x  + 1;
    while (sx < this->size && this->map.at(sx).at(y) == c )
        sx++;
    if (sx < this->size && this->map.at(sx).at(y) == c2)
        ret++;
    return ret;
}

BoardGomoku operator<(BoardGomoku & dest, BoardGomoku &src)
{
	//Cette fonction inverse le board
	dest.setSize(src.getSize());

	auto map = src.getMap();
	std::vector<std::vector<char>> _v;
    int x = 0;

	for (auto it = map.begin(); it != map.end(); it++)
	{
		_v.emplace_back();
		for (auto it2 = (*it).begin(); it2 != (*it).end() ; it2++)
		{
			//_v.at(x).push_back((*it2));
			if ((*it2) == '2')
				_v.at(x).push_back('1');
			else if ((*it2) == '1')
				_v.at(x).push_back('2');
			else
				_v.at(x).push_back('0');
		}
		x++;
	}
	dest.setMap(_v);
	return dest;
}