#include <algorithm>
#include <iostream>
#include <string>
#include <cctype>
#include "Parser.h"
#include "Logger.h"
#include "Helper.h"

Parser::Parser()
{
	this->brain = AlphaGomoku();
}


Parser::~Parser()
{
}

std::string Parser::get_response(std::vector<std::string> argv)
{
	if (argv.size() > 0)
		return this->brain.go_map(argv);
	return "ERROR Parsing error (no arg)";
}

void Parser::parse_input()
{
	std::string line;

	while (1)
	{
		std::getline(std::cin, line);
		std::vector<std::string> argv = Helper::split(line, ' ');
		std::string ret = this->get_response(argv);
		if (!ret.empty())
			std::cout << ret << std::endl;
	}
}
