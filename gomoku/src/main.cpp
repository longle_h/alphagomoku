#include <iostream>
#include <ctime>
#include <Logger.h>
#include "Parser.h"

int main() {
    std::srand(std::time(0));
    Parser p = Parser();
    try {
        p.parse_input();
    } catch (std::exception e)
    {
        Logger::other_log(e.what());
        std::cerr << e.what() << std::endl;
    }
    return (0);
}