#pragma once

#include <vector>
#include <string>
#include "AlphaGomoku.h"

class Parser
{
public:
	Parser();
	virtual ~Parser();
	//Var
public:
	AlphaGomoku brain;

	//Func
public:
	std::string get_response(std::vector<std::string> argv);
	void parse_input();
};

