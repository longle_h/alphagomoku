#pragma once

#include <unordered_map>
#include <functional>
#include "BoardGomoku.h"

class AlphaGomoku
{
public:
	AlphaGomoku();
	virtual ~AlphaGomoku();
	typedef std::string (AlphaGomoku::*func)(std::vector<std::string>);

	//Var
private:
	std::unordered_map <std::string, func> call_map;
	unsigned int map_size;
	BoardGomoku _board;
	char		my_char;
	char		other_char;
	bool		board_state;

	//Function
public:
	std::unordered_map <std::string, func> get_call_map() const;
	std::string brain_start(std::vector<std::string> argv);
	std::string go_map(std::vector<std::string> argv);
	std::string brain_info(std::vector<std::string> argv);
	std::string brain_turn(std::vector<std::string> argv);
	std::string brain_begin(std::vector<std::string> argv);
	std::string brain_end(std::vector<std::string> argv);
	std::string brain_restart(std::vector<std::string> argv);
	std::string brain_recstart(std::vector<std::string> argv);
	std::string brain_takeback(std::vector<std::string> argv);
	std::string brain_play(std::vector<std::string> argv);
	std::string brain_about(std::vector<std::string> argv);
	std::string brain_board(std::vector<std::string> argv);

private:
	std::string pos_to_string(unsigned int x, unsigned int y);
	void manage_minmax(Node & _baseNode);
	int get_index_to_play(Node _baseNode);
	int get_min(Node _baseNode);
};

