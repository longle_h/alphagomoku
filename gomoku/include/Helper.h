#pragma once

#include <vector>
#include <string>
#include <sstream>

class Helper
{
public:
	template<typename Out>
	static void split(const std::string &s, char delim, Out result) {
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			*(result++) = item;
		}
	}

	static std::vector<std::string> split(const std::string &s, char delim) {
		std::vector<std::string> elems;
		Helper::split(s, delim, std::back_inserter(elems));
		return elems;
	}
};