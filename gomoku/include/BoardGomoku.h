#pragma once

#include <vector>
#include <iostream>
#include "Node.h"

class BoardGomoku
{
public:
	BoardGomoku();
	~BoardGomoku();

	//Var
private:
	std::vector<std::vector<char>> map;
	int size;

	//Function
public:
	void fillMap(const  int & map_size);
	void put_on_coord(const  int & x, const  int & y, const char & c) throw (std::logic_error);
	void dumpMap();
	bool is_free(const  int & x, const  int & y);
	void get_possible_turn(Node & base_node, bool evaluate);
	 int getSize() const;
	std::vector<std::vector<char>> getMap() const;
	void setMap(std::vector<std::vector<char>> _v);
	void setSize( int size);
    int evaluate( int x,  int y, bool evaluate);



private:
	void add_adjacent_node( int x,  int y, Node & base_node, bool evaluate);
	bool non_existing_node( int x,  int y, Node & base_node);
	bool is_win( int x,  int y);
	int check_number_diagonal_asc( int x,  int y, char c);
	int check_number_diagonal_desc( int x,  int y, char c);
	int check_number_vertical( int x,  int y, char c);
	int check_number_horizontal( int x,  int y, char c);
	int check_vertical_surround( int x,  int y, char i, char i1);
	int check_diag_asc_surround( int x,  int y, char c, char c2);
	int check_diag_desc_surrond( int x,  int y, char c, char c2);
	int check_horizontal_surround( int x,  int y, char c, char c2);
};

BoardGomoku operator<(BoardGomoku & dest, BoardGomoku &src);