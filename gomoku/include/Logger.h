#pragma once

#include <string>
#include <iostream>
#include <fstream>

#define FILE_LOG "./AlphaGomoku.log.txt"

class Logger
{
public:
	Logger();
	~Logger();

	//Variable

public:
	static std::fstream open_file();
	static void message_received(const std::string& mess);
	static void message_send(const std::string& mess);
	static void other_log(const std::string& mess);
};

