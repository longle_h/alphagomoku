#pragma once

#include <vector>

struct Node
{
	unsigned int x;
	unsigned int y;
	int value;

	Node(unsigned int _x, unsigned int _y) { x = _x; y = _y; value = -1; }
	std::vector<Node> children;
};